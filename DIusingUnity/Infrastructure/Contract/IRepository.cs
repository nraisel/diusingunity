﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIusingUnity.Infrastructure.Contract
{
    public interface IRepository<T>
    {
        IList<T> Get();
        T getById(string Id);
        T Add(T t);
    }
}
