﻿using DIusingUnity.Infrastructure.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DIusingUnity.Models;
using System.Net.Http;
using System.Xml.Serialization;
using System.IO;
using System.Configuration;
using System.Web.Script.Serialization;
using DIusingUnity.DTO;
using System.Net.Http.Headers;

namespace DIusingUnity.Infrastructure.Realization
{
    public class PersonRepository : IPersonRepository
    {
        public Person Add(Person t)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            using (HttpClient client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMilliseconds(Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]));
                string uri = ConfigurationManager.AppSettings["Uri"] + "/AddPerson";
                response = client.PostAsJsonAsync(uri, t).Result;
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var result = serializer.Deserialize<Person>(response.Content.ReadAsStringAsync().Result);
            return result;
        }

        public IList<Person> Get()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var person = new Person();
            using (HttpClient httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders.Accept.Add(
                //    new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.Timeout = TimeSpan.FromMilliseconds(Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]));
                string uri = ConfigurationManager.AppSettings["Uri"];
                response = httpClient.GetAsync(uri).Result;
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var result = serializer.Deserialize<List<Person>>(response.Content.ReadAsStringAsync().Result);

            
            return result;
        }

        public Person getById(string Id)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var person = new Person();
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.Timeout = TimeSpan.FromMilliseconds(Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]));
                string uri = ConfigurationManager.AppSettings["Uri"] + "/GetById/" + Id;
                response = httpClient.GetAsync(uri).Result;
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Deserialize<Person>(response.Content.ReadAsStringAsync().Result);
        }
    }
}