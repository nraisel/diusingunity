﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIusingUnity.Models
{
    public class FlightRequest
    {
        public string flightNumber { get; set; }
        public string origin { get; set; }
        public string flightDate { get; set; }
    }
}