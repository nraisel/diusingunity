﻿using DIusingUnity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIusingUnity.DTO
{
    public class FligthInfoResponse
    {
        public PassengerSummary PassengerSummary { get; set; }
        public string ErrorMessage { get; set; }
    }
}