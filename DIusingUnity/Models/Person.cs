﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIusingUnity.Models
{
    public class Person
    {
        public virtual string Id { get; set; }
        public virtual string Name { get; set; }
        public virtual int Age { get; set; }
    }
}