﻿using DIusingUnity.Infrastructure.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DIusingUnity.Models;
using System.Configuration;
using System.Net.Http;
using DIusingUnity.DTO;

namespace DIusingUnity.Infrastructure.Realization
{
    public class PassengerRepository : IPassengerRepository
    {
        public PassengerFlightInfo Add(PassengerFlightInfo t)
        {
            return null;
        }

        public IList<PassengerFlightInfo> Get()
        {
            return null;
        }

        public PassengerFlightInfo getById(string Id)
        {
            throw new NotImplementedException();
        }

        public FligthInfoResponse getPassengerList(FlightRequest flightRequest)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var queryString = "fltNumber=" + flightRequest.flightNumber +
                                    "&fltDate=" + flightRequest.flightDate +
                                    "&depAirportCd=" + flightRequest.origin.ToUpper() +
                                    "&appId=MYFLIGHT&channelId=HTTP&deviceId=MOBILE&version=2.0";
                using (HttpClient client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromMilliseconds(double.Parse(ConfigurationManager.AppSettings["TimeOut"]));
                    string uri = ConfigurationManager.AppSettings["CepUrl"] + "?" + queryString.Trim();
                    response = client.GetAsync(uri).Result;
                }
                var result = XmlSerializerHelper.XmlDeserializeFromString<PassengerSummary>(response.Content.ReadAsStringAsync().Result);
                return new FligthInfoResponse { PassengerSummary = result, ErrorMessage = "" } ;
            }
            catch (Exception ex)
            {
                return new FligthInfoResponse { PassengerSummary = null, ErrorMessage = ex.Message };
            }
        }
    }
}