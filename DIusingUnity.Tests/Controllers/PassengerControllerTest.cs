﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DIusingUnity.Infrastructure.Realization;
using DIusingUnity.Models;

namespace DIusingUnity.Tests.Controllers
{
    [TestClass]
    public class PassengerControllerTest
    {
        [TestMethod]
        public void GetPassengerFlighInfoListTest()
        {
            var flightRequest = new FlightRequest { flightNumber = "881", flightDate = "22082017", origin = "ORD" };
            var repo = new PassengerRepository();
            var result = repo.getPassengerList(flightRequest);
            Assert.AreEqual("OQ6ZMQ", ((PassengerFlightInfo)result.PassengerSummary.Item).PassengerFlightLeg[0].recLoc);
        }
    }
}
