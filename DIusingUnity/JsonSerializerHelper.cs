﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace DIusingUnity
{
    public class JsonSerializerHelper
    {
        /// <summary>
        /// JSON Serialization
        /// </summary>
        public static string Serialize<T>(T t)
        {
            DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(T));
            MemoryStream memoryStream = new MemoryStream();
            dataContractJsonSerializer.WriteObject(memoryStream, t);
            string jsonString = Encoding.UTF8.GetString(memoryStream.ToArray());
            memoryStream.Close();
            return jsonString;
        }
        //public static string Serialize(this object obj)
        //{
        //    System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        //    //serializer.RegisterConverters(new JavaScriptConverter[] { new SectionConverter(), new SeatMapScreenConverter() });
        //    return serializer.Serialize(obj);
        //}

        /// <summary>
        /// JSON Deserialization
        /// </summary>
        public static T Deserialize<T>(string strJSON)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            //serializer.RegisterConverters(new JavaScriptConverter[] { new SectionConverter(), new SeatMapScreenConverter() });
            return serializer.Deserialize<T>(strJSON);
        }

        public static T DeserializeUseContract<T>(string jsonString)
        {
            DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(T));
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)dataContractJsonSerializer.ReadObject(memoryStream);
            return obj;
        }
    }
}