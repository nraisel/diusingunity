﻿using DIusingUnity.DTO;
using DIusingUnity.Infrastructure.Contract;
using DIusingUnity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace DIusingUnity.Controllers
{
    public class PassengerController : ApiController
    {
        private IPassengerRepository _passengerRepository;
        public PassengerController(IPassengerRepository passengerRepository)
        {
            _passengerRepository = passengerRepository;
        }
        // GET: Passenger
        public ActionResult Get()
        {
            return null;
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult GetPassengerFlightInfoList(FlightRequest flightRequest)
        {
            FligthInfoResponse result = null;
            var _flightRequest = new FlightRequest
            {
                flightNumber = flightRequest.flightNumber,
                flightDate = flightRequest.flightDate,
                origin = flightRequest.origin
            };
            Task[] tasks = new Task[] {
                Task.Factory.StartNew(() => {
                    result = _passengerRepository.getPassengerList(_flightRequest);
                })
            };
            Task.WaitAll(tasks);
            
            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }
    }
}