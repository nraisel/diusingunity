# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Web Api 2 Project. 
Objective:  Implement DI using Unity. 
Also, apart from using Unity for dependency injection, we used this project to GetJsonAsync, PostJsonAsync and GetXmlAsync information from external services. One external service was a Rest API exposing functionality as Json and the other service is an XML service which exposes information as xml using soap protocol.
The external service is ExternalServiceApi which is located inside the repository HttpClientUseJsonAndXml.

To run this service and make use of the ExternalServiceApi you need to clone both repositories (DIusingUnity and HttpClientUseJsonAndXml) and run them in IIS locally.


* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact