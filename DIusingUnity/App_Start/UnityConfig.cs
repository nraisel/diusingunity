using System;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using DIusingUnity.Infrastructure.Contract;
using DIusingUnity.Infrastructure.Realization;
using System.Web.Http;

namespace DIusingUnity.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }

        private static void RegisterTypes(UnityContainer container)
        {
            container.RegisterType<IPersonRepository, PersonRepository>();
            container.RegisterType<IPassengerRepository, PassengerRepository>();
        }
    }
}
