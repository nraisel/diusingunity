﻿using DIusingUnity.DTO;
using DIusingUnity.Infrastructure.Contract;
using DIusingUnity.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace DIusingUnity.Controllers
{
    public class PersonController : ApiController
    {
        private IPersonRepository _personRepository;
        public PersonController(IPersonRepository personRepository)
        {
            _personRepository = personRepository;
        }

        [System.Web.Http.HttpGet]
        // GET: Person
        public IHttpActionResult GetAll()
        {
            var result = new List<Person>();
            Task[] tasks = new Task[] {
                Task.Factory.StartNew(() => {
                    result = _personRepository.Get().ToList();
                })
            };
            Task.WaitAll(tasks);

            return Ok(new PersonList(result));
        }

        [System.Web.Http.HttpGet]
        public IHttpActionResult GetById(string id)
        {
            return Ok(Task.Factory.StartNew(() => { return _personRepository.getById(id); }));
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult AddPerson(Person person)
        {
            var result = new Person();
            Task[] tasks = new Task[] {
                Task.Factory.StartNew(() => {
                    result = _personRepository.Add(person);
                })
            };
            Task.WaitAll(tasks);

            return Ok(result);
        }
    }
}