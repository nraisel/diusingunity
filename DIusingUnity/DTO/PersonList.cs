﻿using DIusingUnity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIusingUnity.DTO
{
    [Serializable]
    public class PersonList
    {
        public List<Person> People;
        public PersonList(List<Person> people)
        {
            People = people;
        }
    }
}