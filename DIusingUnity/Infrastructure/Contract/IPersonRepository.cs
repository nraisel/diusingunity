﻿using DIusingUnity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIusingUnity.Infrastructure.Contract
{
    public interface IPersonRepository : IRepository<Person>
    {

    }
}
